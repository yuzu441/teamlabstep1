# teamlabオンラインスキルアップチャレンジ Step1

--------------------------------------------------

TODOリスト（フロントエンドバージョン)

[チームラボオンラインスキルアップチャレンジ](http://team-lab.github.io/skillup-nodejs/)  
[Step1 最終課題](http://team-lab.github.io/skillup-nodejs/1/8.html)

## 機能
指定された標準機能と自分でつけた追加機能から成り立ってます

###動作環境
以下のブラウザは動作確認済みです

* Google Chrome 41.0.2272.76 (64-bit)
* Firefox Developer Edition 38.0a2

### 標準機能
* 投稿はチェックと内容からなる。
* 投稿データはローカルストレージに保存する。
* 新しい投稿ほど上に表示される。
* HTMLタグが入力された場合はエスケープする。

###追加機能
* チェックをつけると文字に線が引かれる
* 完了済みのタスクを消す

## 作成環境と使用技術
使用したIDEとフレームワークとか

###作成環境
[WebStorm](https://www.jetbrains.com/webstorm/)  
学生ライセンスさまさま

### 使用技術
* knockout.js -[http://knockoutjs.com/](http://knockoutjs.com/)
* Font Awesome - [http://fortawesome.github.io/Font-Awesome/](http://fortawesome.github.io/Font-Awesome/)
* html5-doctor-reset-stylesheet - [http://www.cssreset.com/scripts/html5-doctor-css-reset-stylesheet/](http://www.cssreset.com/scripts/html5-doctor-css-reset-stylesheet/)
