var TodoViewModel = (function() {
  "use strict";

  var self;

  function IndexViewModel(model) {
    self = this;
    self.model = model;
    self.taskList = model.taskList;

    self.visibleAddTaskPopup = ko.observable(false);
    self.inputTaskName = ko.observable("");
    self.addBtnEnable = ko.pureComputed(function() {
      return self.inputTaskName().length > 0;
    });
  }

  IndexViewModel.prototype.showAddTaskPopup = showAddTaskPopup;
  IndexViewModel.prototype.addTask = addTask;
  IndexViewModel.prototype.storeTask = storeTask;
  IndexViewModel.prototype.clickTask = clickTask;
  IndexViewModel.prototype.doneRemove = doneRemove;

  function showAddTaskPopup() {
    var currentStatus = self.visibleAddTaskPopup();
    self.visibleAddTaskPopup(!currentStatus);
  }

  function addTask() {
    self.model.addTask(self.inputTaskName());
    self.inputTaskName("");
    self.visibleAddTaskPopup(false);
  }

  function storeTask() {
    self.model.storeTask();
  }

  function clickTask() {
    var currentTask = this.status();
    this.status(!currentTask);
  }

  function doneRemove() {
    self.model.clearTask();
  }

  return IndexViewModel;
})();