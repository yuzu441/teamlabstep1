var TaskModel = (function(){
  "use strict";

  var self;

  function TaskModel(name, date, status) {
    self = this;
    self.name = ko.observable(name);
    self.date = ko.observable(date);
    self.status = ko.observable(status);//false->未完了, true->完了
  }

  return TaskModel;
})();