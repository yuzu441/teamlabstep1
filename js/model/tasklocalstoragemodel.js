var TaskLocalStorageModel = (function() {
  "use strict";

  var self;

  function TaskLocalStorageModel() {
    self = this;
    self.taskList = ko.observableArray();

    readTasks();
  }

  TaskLocalStorageModel.prototype.addTask = addTask;
  TaskLocalStorageModel.prototype.storeTask = storeTasks;
  TaskLocalStorageModel.prototype.clearTask = clearTask;

  function storeTasks() {
    var json = ko.toJSON(self.taskList());
    localStorage.setItem("tasklist", json)
  }

  function readTasks() {
    var readList = ko.utils.parseJson(localStorage.getItem("tasklist"));
    var list = ko.utils.arrayMap(readList, function(item) {
      return new TaskModel(item.name, new Date(item.date), item.status);
    });

    list.sort(function(first, second) {
      return first.date() == second.date() ?
          0 : (first.date() > second.date() ? -1 : 1);
    });

    self.taskList(list);
  }

  function addTask(name) {
    var task = new TaskModel(name, new Date(), false);
    self.taskList.splice(0, 0, task);
  }

  function clearTask() {
    var doingTasks = ko.utils.arrayFilter(self.taskList(), function(task){
      return task.status() === false;
    });
    self.taskList(doingTasks);
  }

  return TaskLocalStorageModel;
})();